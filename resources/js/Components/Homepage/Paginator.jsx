import { Link } from "@inertiajs/inertia-react";

const Paginator = ({ meta }) => {
    const prev = meta.links[0].url;
    const next = meta.links[meta.links.length - 1].url;
    const current = meta.current_page;
    return (
        <div className="btn-group m-4">
            {prev && (
                <Link href={prev} className="btn text-primary-content">
                    «
                </Link>
            )}
            <Link className="btn text-primary-content">{current}</Link>
            {next && (
                <Link href={next} className="btn text-primary-content">
                    »
                </Link>
            )}
        </div>
    );
};

export default Paginator;
